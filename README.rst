=======================================
mlbfilt: multi-line output block filter
=======================================

``mlbfilt`` can filter one or more multi-line text blocks from the
output of a command or file. It can be conveniently used either as an
error filter or to match complex multi-line sequences.

``mlbfilt`` is geared towards usage within interactive shell sessions by
handling colorized output sensibly, flushing as soon as possible and
being able to handle multiple large patterns efficiently.


Usage and examples
==================

Assuming the following input file::

  Line 1
  Line 2
  Line 3
  Line 4

you can use ``mlbfilt`` to remove line 2 and 3 by creating the following
filter::

  Line 2
  Line 3

and running it as follows:

.. code:: console

  $ mlbfilt filter.txt < input.txt
  Line 1
  Line 4

Multiple filters can be defined in the same file, separating each by
using "^L" (ASCII 12) in a stand-alone line::

  Line 2
  Line 3
  ^L
  Line 4

``mlbfilt`` is *greedy*, filtering the longest matching output sequence
*first*. Assuming the same input file as above, and the filter::

  Line 1
  ^L
  Line 1
  Line 2
  Line 3

the output will always result in just::

  Line 4

irregardless of the filter order, since the longest pattern (Line 1-3)
is removed even if the pattern ``Line 1`` is fully contained within it
and terminates earlier.

The default block separator can be changed by using the ``--delimiter``
or ``-d`` command flag. An empty delimiter will read the entire file as
a single block. Such behavior is especially useful to load multiple
files containing a single pattern each:

.. code:: console

  $ cmd ... | mlbfilt -d '' pat-*


Matching with regular expressions
=================================

Sometimes the output that you want to filter includes portions that
vary. ``mlbfilt`` can perform matches based on regular expressions on a
line-by-line basis, meaning that fixed strings and regular expressions
can be intermixed in the same block rule for maximum efficiency.

If a line in the filter file starts with a forward slash, the rest of
the line is interpreted as a fully anchored regular expression::

  WARNING:
  /line \d+: file not found

The above will match (and filter) output such as::

  WARNING:
  line 142: file not found

The regex prefix can be changed with the ``-r`` flag if needed, or
disabled completely with ``-r ''`` to avoid interpreting forward slashes
as regular expressions.

When used with ``--invert`` or ``-i`` (which complements the output by
filtering non-matching text), ``mlbfilt`` effectively becomes a
multi-line grep equivalent.

Regular expressions match slower, so they should be avoided when
possible by using fixed strings instead.


Filtering interactive output
============================

``mlbfilt`` ignores control-sequences and trailing whitespace by default
when matching the output filters. This means that the simple filter::

  error: some error message

will match correctly even if "error" has been colorized and/or padded
with invisible whitespace. You can disable this behavior (for example,
to match control sequences *verbatim*) by using the
``--no-ign-ctrl``/``-c`` flag and ``--no-ign-trailing``/``-t``.

Some commands disable coloring when their output is redirected, even
though ``mlbfilt`` emits unmatched output as-is. In such cases you might
need to force these tools to colorize the output using
``--color=always`` or similar flags.

When redirecting, many commands switch their output from being
line-buffered to being block-buffered. This results in the output being
delayed which is frequently not desired. Some tools support flags to
force output flushing (such as ``sed -u``) while others can be coerced
to do so using coreutil's ``stdbuf``:

.. code:: console

  $ stdbuf -oL command ... | mlbfilt ...

``mlbfilt`` flushes its output each line by default. When not used
interactively you can disable this behavior with ``-b`` for increased
throughput.


Installation
============

``mlbfilt`` is currently a stand-alone python script. Install by copying
the script into ``PATH``. Python 3 is required.


Authors and Copyright
=====================

| Copyright(c) 2021 by wave++ "Yuri D'Elia" <wavexx@thregr.org>
| Distributed under the GNU GPLv3+ license, WITHOUT ANY WARRANTY.

``mlbfilt``'s GIT repository is publicly accessible at:

https://gitlab.com/wavexx/mlbfilt
